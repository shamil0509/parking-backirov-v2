package ru.bakirov.parking.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import ru.bakirov.parking.entity.ParkingSpace;

@Component
public interface ParkingSpaceRepository extends JpaRepository<ParkingSpace, Integer> {
}
