package ru.bakirov.parking.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import ru.bakirov.parking.entity.Car;

@Component
public interface CarRepository extends JpaRepository<Car, Integer> {
}
