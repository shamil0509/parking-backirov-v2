package ru.bakirov.parking.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bakirov.parking.dto.CarDTO;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String brand;
    @Column(nullable = false)
    private String model;
    @Column(nullable = false)
    private String number;
    @Column(nullable = false)
    private String color;
    @Column(nullable = false)
    private String photo;

    @OneToOne
    @JoinColumn(name = "parking_space_id", referencedColumnName = "id")
    private ParkingSpace parkingSpace;

    public Car(String brand, String model, String number, String color, String photo) {
        this.brand = brand;
        this.model = model;
        this.number = number;
        this.color = color;
        this.photo = photo;
    }

    public Car(String number) {
        this.number = number;
    }

    // "id" and "parkingSpace" are taken from oldCar.
    public Car(Car car, CarDTO carDTO) {
        this.id = car.getId();

        this.brand = carDTO.getBrand();
        this.model = carDTO.getModel();
        this.number = carDTO.getNumber();
        this.color = carDTO.getColor();
        this.photo = carDTO.getPhoto();

        this.parkingSpace = car.getParkingSpace();
    }

    public Car(CarDTO carDTO) {
        this.brand = carDTO.getBrand();
        this.model = carDTO.getModel();
        this.number = carDTO.getNumber();
        this.color = carDTO.getColor();
        this.photo = carDTO.getPhoto();
    }
}

