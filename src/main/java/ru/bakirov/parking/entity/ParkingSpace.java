package ru.bakirov.parking.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bakirov.parking.dto.ParkingSpaceDTO;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ParkingSpace {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String carNumber;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ParkingSpaceStatus spaceStatus;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ParkingPaymentStatus paymentStatus;

    public ParkingSpace(String carNumber, ParkingSpaceStatus spaceStatus,
                        ParkingPaymentStatus paymentStatus) {

        this.carNumber = carNumber;
        this.spaceStatus = spaceStatus;
        this.paymentStatus = paymentStatus;
    }

    public ParkingSpace(ParkingSpaceDTO dto) {

        this.carNumber = dto.getCarNumber();
        this.spaceStatus = dto.getSpaceStatus();
        this.paymentStatus = dto.getPaymentStatus();
    }
}
