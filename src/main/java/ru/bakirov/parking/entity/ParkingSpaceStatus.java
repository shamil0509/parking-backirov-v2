package ru.bakirov.parking.entity;

public enum ParkingSpaceStatus {
    OCCUPIED, FREE, OUT_OF_ORDER
}
