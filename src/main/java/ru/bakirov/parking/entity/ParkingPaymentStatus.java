package ru.bakirov.parking.entity;

public enum ParkingPaymentStatus {
    PAYED, NOT_PAYED, OVERDUE
}
