package ru.bakirov.parking.service;

import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.bakirov.parking.entity.User;

@Data
public class RegistrationForm {

    private String username;
    private String password;

    public User toUser(PasswordEncoder passwordEncoder) {

        return new User(username, passwordEncoder.encode(password));
    }
}
