package ru.bakirov.parking.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.repository.JpaRepository;

public class ValidationUtil {

    public static void assertNotBlank(String input, String message) {
        if (StringUtils.isBlank(input)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void assertNotNull(Object input, String message) {
        if (input == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void assertExistsById(JpaRepository repository, int id, String message) {
        if (!repository.existsById(id)) {
            throw new IllegalArgumentException(message);
        }
    }
}
