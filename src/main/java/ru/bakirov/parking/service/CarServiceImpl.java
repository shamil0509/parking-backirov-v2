package ru.bakirov.parking.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.bakirov.parking.dao.CarRepository;
import ru.bakirov.parking.dao.ParkingSpaceRepository;
import ru.bakirov.parking.dto.CarDTO;
import ru.bakirov.parking.entity.Car;
import ru.bakirov.parking.entity.ParkingSpace;
import ru.bakirov.parking.entity.ParkingSpaceStatus;

import java.util.List;

import static ru.bakirov.parking.service.ValidationUtil.assertExistsById;
import static ru.bakirov.parking.service.ValidationUtil.assertNotBlank;

@Service
@AllArgsConstructor
public class CarServiceImpl implements CarService {

    private CarRepository carRepository;
    private ParkingSpaceRepository parkingSpaceRepository;

    @Override
    public void createCar(CarDTO carDTO) {

        assertNotBlank(carDTO.getBrand(), "Brand cannot be empty");
        assertNotBlank(carDTO.getModel(), "Model cannot be empty");
        assertNotBlank(carDTO.getNumber(), "Number cannot be empty");
        assertNotBlank(carDTO.getColor(), "Color cannot be empty");
        assertNotBlank(carDTO.getPhoto(), "Photo cannot be empty");

        Car car = new Car(carDTO);
        carRepository.save(car);
    }

    public List<Car> getCarsList() {
        return carRepository.findAll();
    }

    @Override
    public void editCar(Integer id, CarDTO carDTO) {

        Car oldCar = carRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Car with id " + id + " not found."));
        Car newCar = new Car(oldCar, carDTO);
        carRepository.save(newCar);
    }

    @Override
    public void assignParkingSpace(Integer carId, Integer parkingSpaceId) {

        assertExistsById(carRepository, carId, "Car with id " + carId + " not found.");
        assertExistsById(parkingSpaceRepository, parkingSpaceId,
                "Parking space with id " + parkingSpaceId + " not found.");

        Car car = carRepository.findById(carId)
                .orElseThrow(() -> new IllegalArgumentException(
                        "Car with id " + carId + " not found."));

        ParkingSpace parkingSpace = parkingSpaceRepository.findById(parkingSpaceId)
                .orElseThrow(() -> new IllegalArgumentException(
                        "Car with id " + parkingSpaceId + " not found."));

        car.setParkingSpace(parkingSpace);
        carRepository.save(car);

        parkingSpace.setCarNumber(car.getNumber());
        parkingSpace.setSpaceStatus(ParkingSpaceStatus.OCCUPIED);
        parkingSpaceRepository.save(parkingSpace);
    }

    @Override
    public void deleteCar(Integer id) {

        assertExistsById(carRepository, id, "Car with id " + id + " not found.");

        carRepository.deleteById(id);
    }
}
