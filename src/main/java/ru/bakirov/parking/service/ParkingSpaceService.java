package ru.bakirov.parking.service;

import org.springframework.stereotype.Component;
import ru.bakirov.parking.dto.ParkingSpaceDTO;
import ru.bakirov.parking.entity.ParkingPaymentStatus;
import ru.bakirov.parking.entity.ParkingSpaceStatus;

@Component
public interface ParkingSpaceService {
    void createParkingSpace(ParkingSpaceDTO dto);
    void deleteParkingSpace(Integer id);
    void changeSpaceStatus(Integer id, ParkingSpaceStatus status);
    void changePaymentStatus(Integer id, ParkingPaymentStatus status);
}
