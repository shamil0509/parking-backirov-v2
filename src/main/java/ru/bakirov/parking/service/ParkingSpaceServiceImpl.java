package ru.bakirov.parking.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.bakirov.parking.dao.ParkingSpaceRepository;
import ru.bakirov.parking.dto.ParkingSpaceDTO;
import ru.bakirov.parking.entity.ParkingPaymentStatus;
import ru.bakirov.parking.entity.ParkingSpace;
import ru.bakirov.parking.entity.ParkingSpaceStatus;

import static ru.bakirov.parking.service.ValidationUtil.assertExistsById;
import static ru.bakirov.parking.service.ValidationUtil.assertNotNull;

@Service
@AllArgsConstructor
public class ParkingSpaceServiceImpl implements ParkingSpaceService {

    private ParkingSpaceRepository parkingSpaceRepository;

    @Override
    public void createParkingSpace(ParkingSpaceDTO dto) {

        assertNotNull(dto.getSpaceStatus(), "Space status cannot be empty");
        assertNotNull(dto.getPaymentStatus(), "Payment status cannot be empty");

        ParkingSpace parkingSpace = new ParkingSpace(dto);
        parkingSpaceRepository.save(parkingSpace);
    }

    @Override
    public void deleteParkingSpace(Integer id) {
        assertExistsById(
                parkingSpaceRepository, id, "Parking space with id " + id + " not found.");

        parkingSpaceRepository.deleteById(id);
    }

    @Override
    public void changeSpaceStatus(Integer id, ParkingSpaceStatus status) {
        assertExistsById(
                parkingSpaceRepository, id, "Parking space with id " + id + " not found.");

        ParkingSpace parkingSpace = parkingSpaceRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Car with id " + id + " not found."));

        parkingSpace.setSpaceStatus(status);
    }

    @Override
    public void changePaymentStatus(Integer id, ParkingPaymentStatus status) {
        assertExistsById(
                parkingSpaceRepository, id, "Parking space with id " + id + " not found.");

        ParkingSpace parkingSpace = parkingSpaceRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Car with id " + id + " not found."));

        parkingSpace.setPaymentStatus(status);
    }
}
