package ru.bakirov.parking.service;

import org.springframework.stereotype.Component;
import ru.bakirov.parking.dto.CarDTO;
import ru.bakirov.parking.entity.Car;
import java.util.List;

@Component
public interface CarService {
    void createCar(CarDTO carDTO);
    List<Car> getCarsList();
    void editCar(Integer id, CarDTO newCar);
    void assignParkingSpace(Integer carId, Integer parkingSpaceId);
    void deleteCar(Integer id);
}
