package ru.bakirov.parking.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.bakirov.parking.dto.ParkingSpaceDTO;
import ru.bakirov.parking.entity.ParkingPaymentStatus;
import ru.bakirov.parking.entity.ParkingSpaceStatus;
import ru.bakirov.parking.service.ParkingSpaceService;

@Controller
@AllArgsConstructor
@RequestMapping("/parking-spaces")
public class ParkingSpaceController {

    ParkingSpaceService parkingSpaceService;

    @PostMapping
    public void createParkingSpace(@RequestBody ParkingSpaceDTO dto) {
        parkingSpaceService.createParkingSpace(dto);
    }

    @DeleteMapping("/{id}")
    public void deleteParkingSpace(@PathVariable Integer id) {
        parkingSpaceService.deleteParkingSpace(id);
    }

    @PatchMapping("/space-status")
    public void changeSpaceStatus(@RequestParam Integer id, @RequestBody ParkingSpaceStatus status) {
        parkingSpaceService.changeSpaceStatus(id, status);
    }

    @PatchMapping("/payment-status")
    public void changePaymentStatus(@RequestParam Integer id, @RequestBody ParkingPaymentStatus status) {
        parkingSpaceService.changePaymentStatus(id, status);
    }
}
