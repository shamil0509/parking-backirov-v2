package ru.bakirov.parking.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.bakirov.parking.dto.CarDTO;
import ru.bakirov.parking.entity.Car;
import ru.bakirov.parking.service.CarService;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/cars")
public class CarController {

    CarService carService;

    @PostMapping
    public void createCar(@RequestBody CarDTO car) {
        carService.createCar(car);
    }

    @GetMapping
    public ResponseEntity<List<Car>> getCarsList() {
        return new ResponseEntity<>(carService.getCarsList(), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public void editCar(@PathVariable Integer id, @RequestBody CarDTO carDTO) {
        carService.editCar(id, carDTO);
    }

    @PatchMapping("/parking-space-assigner")
    public void assignParkingSpace(@RequestParam Integer carId,
                                   @RequestParam Integer parkingSpaceId) {
        carService.assignParkingSpace(carId, parkingSpaceId);
    }

    @DeleteMapping("/{id}")
    public void deleteCar(@PathVariable Integer id) {
        carService.deleteCar(id);
    }

}
