package ru.bakirov.parking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bakirov.parking.entity.ParkingPaymentStatus;
import ru.bakirov.parking.entity.ParkingSpaceStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParkingSpaceDTO {
    private int id;
    private String carNumber;
    private ParkingSpaceStatus spaceStatus;
    private ParkingPaymentStatus paymentStatus;

    public ParkingSpaceDTO(String carNumber, ParkingSpaceStatus spaceStatus,
                           ParkingPaymentStatus paymentStatus) {
        this.carNumber = carNumber;
        this.spaceStatus = spaceStatus;
        this.paymentStatus = paymentStatus;
    }
}
