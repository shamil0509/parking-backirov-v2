package ru.bakirov.parking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDTO {
    private Integer id;
    private String brand;
    private String model;
    private String number;
    private String color;
    private String photo;

    public CarDTO(String brand, String model, String number, String color, String photo) {
        this.brand = brand;
        this.model = model;
        this.number = number;
        this.color = color;
        this.photo = photo;
    }
}
