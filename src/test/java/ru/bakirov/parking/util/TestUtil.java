package ru.bakirov.parking.util;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import ru.bakirov.parking.dao.CarRepository;
import ru.bakirov.parking.dao.ParkingSpaceRepository;
import ru.bakirov.parking.entity.Car;
import ru.bakirov.parking.entity.ParkingPaymentStatus;
import ru.bakirov.parking.entity.ParkingSpace;
import ru.bakirov.parking.entity.ParkingSpaceStatus;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Component
@AllArgsConstructor
public class TestUtil {

    private CarRepository carRepository;
    private ParkingSpaceRepository parkingSpaceRepository;

    public Example<Car> getCarExample(Car car) {
        Example<Car> example = Example.of(car);
        assertTrue(carRepository.exists(example));
        return example;
    }

    public Example<ParkingSpace> getParkingSpaceExample(ParkingSpace parkingSpace) {
        Example<ParkingSpace> example = Example.of(parkingSpace);
        assertTrue(parkingSpaceRepository.exists(example));
        return example;
    }

    public Car getCarById(int id) {
        Optional<Car> carOptional = carRepository.findById(id);
        assertTrue(carOptional.isPresent());
        return carOptional.get();
    }

    public ParkingSpace getParkingSpaceById(int id) {
        Optional<ParkingSpace> parkingSpaceOptional = parkingSpaceRepository.findById(id);
        assertTrue(parkingSpaceOptional.isPresent());
        return parkingSpaceOptional.get();
    }

    public Car createCarInDatabase(String fieldStubName) {
        Car car = new Car(fieldStubName, fieldStubName, fieldStubName, fieldStubName, fieldStubName);
        carRepository.save(car);
        return car;
    }

    public ParkingSpace createParkingSpaceInDatabase(String fieldStubName) {

        ParkingSpace parkingSpace = new ParkingSpace(
                fieldStubName, ParkingSpaceStatus.OUT_OF_ORDER, ParkingPaymentStatus.NOT_PAYED);
        parkingSpaceRepository.save(parkingSpace);
        return parkingSpace;
    }

    public int getCarId(Example<Car> example) {
        Optional<Car> carOptionalBeforeDelete = carRepository.findOne(example);
        assertTrue(carOptionalBeforeDelete.isPresent());
        return carOptionalBeforeDelete.get().getId();
    }

    public int getParkingSpaceIdFromExample(Example<ParkingSpace> example) {
        Optional<ParkingSpace> parkingSpaceOptional = parkingSpaceRepository.findOne(example);
        assertTrue(parkingSpaceOptional.isPresent());
        return parkingSpaceOptional.get().getId();
    }

    public boolean isNull(List<Car> carsList) {

        boolean isNull = false;

        for (Car car :
                carsList) {
            if (car == null) {
                isNull = true;
                break;
            }
        }

        return isNull;
    }
}
