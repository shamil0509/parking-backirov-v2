package ru.bakirov.parking.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import ru.bakirov.parking.dao.CarRepository;
import ru.bakirov.parking.dto.CarDTO;
import ru.bakirov.parking.entity.Car;
import ru.bakirov.parking.entity.ParkingSpace;
import ru.bakirov.parking.util.TestUtil;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CarServiceImplTest {

    @Autowired
    private CarService carService;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private TestUtil util;

    @Test
    public void createCar_shouldCreateCarInDatabase() {

        // Given
        CarDTO expected = new CarDTO(
                "Ferrari", "Roma", "e231cj", "gray", "new_photo_3");

        // When
        carService.createCar(expected);
        Car carForExample = new Car(expected);
        Example<Car> example = util.getCarExample(carForExample);
        int id = util.getCarId(example);

        Car car = util.getCarById(id);

        CarDTO actual = new CarDTO(
                car.getBrand(), car.getModel(), car.getNumber(), car.getColor(), car.getPhoto());

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void createCar_shouldThrowException_whenSomeFieldInCarDtoIsNull() {

        // Given
        CarDTO carDTO = new CarDTO(
                null, "Roma", "e231cj", "gray", "new_photo_3");

        // Then
        assertThrows(IllegalArgumentException.class, () -> {
            carService.createCar(carDTO);
        });
    }

    @Test
    public void createCar_shouldThrowException_whenSomeFieldInCarDtoIsEmpty() {

        // Given
        CarDTO carDTO = new CarDTO(
                "", "Roma", "e231cj", "gray", "new_photo_3");

        // Then
        assertThrows(IllegalArgumentException.class, () -> {
            carService.createCar(carDTO);
        });
    }

    @Test
    public void createCar_shouldThrowException_whenSomeFieldInCarDtoContainsOnlyWhitespaces() {

        // Given
        CarDTO carDTO = new CarDTO(
                "    ", "Roma", "e231cj", "gray", "new_photo_3");

        // Then
        assertThrows(IllegalArgumentException.class, () -> {
            carService.createCar(carDTO);
        });
    }

    @Test
    public void getCarsList_shouldReturnCarsList() {

        // Given
        Car car = util.createCarInDatabase("TestGetCarsList");

        // When
        List<Car> carsList = carService.getCarsList();
        boolean listIsNull = util.isNull(carsList);

        System.out.println(carsList);

        // Then
        assertFalse(listIsNull);
    }

    @Test
    public void editCar_shouldEditCarInDatabase() {

        // Given
        Car oldCar = util.createCarInDatabase("OldCar");
        Example<Car> example = util.getCarExample(oldCar);
        int oldCarId = util.getCarId(example);

        CarDTO expected = new CarDTO(
                "NewCar", "NewCar", "NewCar", "NewCar", "NewCar");

        // When
        carService.editCar(oldCarId, expected);
        Car newCar = util.getCarById(oldCarId);

        CarDTO actual = new CarDTO(
                newCar.getBrand(), newCar.getModel(), newCar.getNumber(),
                newCar.getColor(), newCar.getPhoto()
        );

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void assignParkingSpace_shouldAssignParkingSpaceToCar() {

        // Given
        Car car = util.createCarInDatabase("TestCar");
        Example<Car> carExample = util.getCarExample(car);
        int carId = util.getCarId(carExample);

        ParkingSpace parkingSpace = util.createParkingSpaceInDatabase("TestParkingSpace");
        Example<ParkingSpace> parkingSpaceExample = util.getParkingSpaceExample(parkingSpace);
        int parkingSpaceId = util.getParkingSpaceIdFromExample(parkingSpaceExample);

        parkingSpace.setCarNumber(car.getNumber());

        Car expected = new Car(carId, car.getBrand(), car.getModel(), car.getNumber(), car.getColor(),
                car.getPhoto(), parkingSpace);

        // When
        System.out.println("Parking space id: "+ parkingSpaceId);

        carService.assignParkingSpace(carId, parkingSpaceId);
        Car actual = util.getCarById(carId);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void deleteCar_shouldDeleteCarFromDatabase() {

        // Given
        Car car = util.createCarInDatabase("TestDeleteCar");
        Example<Car> example = util.getCarExample(car);
        int id = util.getCarId(example);

        // When
        carService.deleteCar(id);

        // Then
        assertFalse(carRepository.exists(example));
    }
}