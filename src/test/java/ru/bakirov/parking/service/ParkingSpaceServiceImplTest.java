package ru.bakirov.parking.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import ru.bakirov.parking.dao.ParkingSpaceRepository;
import ru.bakirov.parking.dto.ParkingSpaceDTO;
import ru.bakirov.parking.entity.ParkingPaymentStatus;
import ru.bakirov.parking.entity.ParkingSpace;
import ru.bakirov.parking.entity.ParkingSpaceStatus;
import ru.bakirov.parking.util.TestUtil;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ParkingSpaceServiceImplTest {

    @Autowired
    private ParkingSpaceService parkingSpaceService;
    @Autowired
    private TestUtil util;
    @Autowired
    private ParkingSpaceRepository parkingSpaceRepository;

    @Test
    void createParkingSpace_shouldCreateParkingSpaceInDatabase() {

        // Given
        ParkingSpaceDTO expected = new ParkingSpaceDTO(
                "TestCreateParkingSpace", ParkingSpaceStatus.FREE, ParkingPaymentStatus.NOT_PAYED);

        // When
        parkingSpaceService.createParkingSpace(expected);
        ParkingSpace parkingSpaceForExample = new ParkingSpace(expected);
        Example<ParkingSpace> example = util.getParkingSpaceExample(parkingSpaceForExample);
        int id = util.getParkingSpaceIdFromExample(example);

        ParkingSpace parkingSpace = util.getParkingSpaceById(id);

        ParkingSpaceDTO actual = new ParkingSpaceDTO(
                parkingSpace.getCarNumber(), parkingSpace.getSpaceStatus(), parkingSpace.getPaymentStatus());

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void createParkingSpace_shouldThrowException_whenSomeFieldWithStatusInParkingSpaceDtoIsNull() {

        // Given
        ParkingSpaceDTO parkingSpaceDTO = new ParkingSpaceDTO(
                "TestCreateParkingSpace", null, ParkingPaymentStatus.NOT_PAYED);

        // Then
        assertThrows(IllegalArgumentException.class, () -> {
            parkingSpaceService.createParkingSpace(parkingSpaceDTO);
        });
    }

    @Test
    void deleteParkingSpace_shouldDeleteParkingSpaceFromDatabase() {

        // Given
        ParkingSpace parkingSpace = util.createParkingSpaceInDatabase("TestDeleteParkingSpace");
        Example<ParkingSpace> example = util.getParkingSpaceExample(parkingSpace);
        int id = util.getParkingSpaceIdFromExample(example);

        // When
        parkingSpaceService.deleteParkingSpace(id);

        // Then
        assertFalse(parkingSpaceRepository.exists(example));
    }

    @Test
    void changeSpaceStatus_shouldChangeSpaceStatusOfParkingSpaceInDatabase() {

        // Given
        ParkingSpace expected = new ParkingSpace("TestChangeSpaceStatus",
                ParkingSpaceStatus.OCCUPIED, ParkingPaymentStatus.NOT_PAYED);

        // When
        ParkingSpaceDTO parkingSpaceForDatabase = new ParkingSpaceDTO("TestChangeSpaceStatus",
                ParkingSpaceStatus.FREE, ParkingPaymentStatus.NOT_PAYED);

        parkingSpaceService.createParkingSpace(parkingSpaceForDatabase);

        int id = getParkingSpaceId(parkingSpaceForDatabase);
        parkingSpaceService.changeSpaceStatus(id, ParkingSpaceStatus.OCCUPIED);

        ParkingSpace parkingSpace = util.getParkingSpaceById(id);
        ParkingSpaceDTO actual = new ParkingSpaceDTO(
                parkingSpace.getCarNumber(), parkingSpace.getSpaceStatus(), parkingSpace.getPaymentStatus());

        // Then
        assertEquals(expected, actual);

    }

    private int getParkingSpaceId(ParkingSpaceDTO input) {
        ParkingSpace parkingSpaceForExample = new ParkingSpace(input);
        Example<ParkingSpace> example = util.getParkingSpaceExample(parkingSpaceForExample);
        return util.getParkingSpaceIdFromExample(example);
    }

    @Test
    void changeParkingPaymentStatus() {
    }
}